from itertools import tee


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    first_it, second_it = tee(iterable)
    next(second_it, None)
    return zip(first_it, second_it)
