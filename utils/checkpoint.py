from collections import namedtuple
import datetime
import json
import os
import glob
import torch


def argfunc(minmaxfunc):
    def inner_func(values, key=None):
        def inner_key(idx):
            value = values[idx]
            if key is None:
                return value
            else:
                return key(value)
        return minmaxfunc(range(len(values)), key=inner_key)
    return inner_func


argmin = argfunc(min)
argmax = argfunc(max)

FILE_LIMIT = 10


class CheckpointManager:
    def __init__(self, checkpoints_path, name):
        self.name = name
        self.path = os.path.join(checkpoints_path, name)
        self.model_path = os.path.join(self.path, 'model')
        self.best_index = None
        self.best_acc = -float('inf')
        self.files = []

        if not os.path.isdir(self.path):
            os.mkdir(self.path)
        if not os.path.isdir(self.model_path):
            os.mkdir(self.model_path)

        self.read_files()

    @property
    def last_model(self):
        if not self.files:
            return None
        return self.files[-1]

    @property
    def best_model(self):
        if not self.files or self.best_index is None:
            return None
        return self.files[self.best_index]

    def read_files(self):
        files = []
        for file in glob.glob(os.path.join(self.path, f'*.json')):
            with open(file, 'r') as fp:
                state = json.load(fp)
            accuracy = state['accuracy']
            training_data = dict(
                file=file,
                accuracy=state['accuracy'],
                epochs=state['epochs'],
                model_filename=state['model_filename']
            )
            files.append(training_data)
        if not files:
            return
        self.best_index = argmin(files, key=lambda x: x['accuracy'])
        self.best_acc = files[self.best_index]['accuracy']

        files.sort(key=lambda td: td['epochs'])
        self.files = files

    @property
    def best_accuracy(self):
        if not self.files:
            return -float('inf')
        return self.files[self.best_index]['accuracy']

    def add_new(self, training_data):
        timestamp = datetime.datetime.now().replace(microsecond=0).isoformat()
        checkpoint_filename = os.path.join(
            self.path, f'{timestamp}.json')
        model_filename = os.path.join(
            self.model_path, f'{timestamp}.dump')

        torch.save({
            'state_dict': training_data['state_dict'],
            'optimizer': training_data['optimizer'],
        }, model_filename)

        checkpoint = {
            'epochs': training_data['epochs'],
            'accuracy': training_data['accuracy'],
            'model_filename': model_filename,
        }

        with open(checkpoint_filename, 'w') as fp:
            json.dump(checkpoint, fp)

        training_data_entry = dict(
            file=checkpoint_filename,
            accuracy=training_data['accuracy'],
            epochs=training_data['epochs'],
            model_filename=model_filename
        )
        # Drop if there are more than FILE_LIMIT files.
        if len(self.files) > FILE_LIMIT:
            # Drop the oldest one that's not the bet
            if self.best_index > 0:
                popped = self.files.pop(0)
            else:
                popped = self.files.pop(1)
            os.remove(popped['model_filename'])
            os.remove(popped['file'])

        if self.files:
            self.best_index = argmin(self.files, key=lambda x: x['accuracy'])

        accuracy = training_data['accuracy']

        if accuracy > self.best_accuracy:
            self.best_index = len(self.files)

        self.files.append(training_data_entry)
