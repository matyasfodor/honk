import math
import random

import numpy as np


def get_magic_list_(expected_len, number):
    whole_value = math.floor(expected_len)
    remainder = expected_len - whole_value
    for _ in range(number):
        if random.random() < remainder:
            yield whole_value + 1
        else:
            yield whole_value


def get_magic_list(expected_len, desired_sum):
    whole_value = math.floor(expected_len)
    number = round(desired_sum / whole_value) + 1
    arr = np.zeros(number, dtype=int)
    arr[0] = whole_value
    for i in range(1, number):
        if (arr.sum() + 1.0) / i < expected_len:
            arr[i] = whole_value + 1
        else:
            arr[i] = whole_value
        if arr.sum() > desired_sum:
            return arr[:i]

# arr1 = np.array([l for l in get_magic_list(5.9, 48)])
# print('20 samples', arr1, arr1.mean(), arr1.sum())

# arr2 = np.array([l for l in get_magic_list(7.3, 87)])
# print('100 samples', arr2, arr2.mean(), arr2.sum())
