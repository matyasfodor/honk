import torch

from .drnn import DynamicRNN

x = [torch.tensor([[1.0, 1.0],
                   [2.0, 2.0],
                   [3.0, 3.0],
                   [4.0, 4.0],
                   [5.0, 5.0]]),

     torch.tensor([[2.5, 2.5]]),

     torch.tensor([[2.2, 2.2],
                   [3.5, 3.5]])]


rnn = torch.nn.LSTM(input_size=2, hidden_size=3, bidirectional=True, batch_first=True)
drnn = DynamicRNN(rnn)

out, (h_n, c_n), last = drnn(x)
print(out)
print(last)
