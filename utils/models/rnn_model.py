from collections import OrderedDict

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from .drnn import DynamicRNN
from .mobilenetv2 import InvertedResidual, conv_bn, conv_1x1_bn
from .utils import SerializableModule

convs = {
    True: lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        nn.Dropout(p=dropout),
    ),
    'one': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        nn.Dropout(p=dropout),
    ),
    'two': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(1, 1, 3, stride=(1, 1)),
        nn.Dropout(p=dropout),
    ),
    'three': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        nn.Dropout(p=dropout),
    ),
    'four': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(1, 1, 3, stride=(1, 2)),
        # F.relu,
        nn.Dropout(p=dropout),
    ),
    'five': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, (3, 8), stride=(2, 1)),
        # F.relu,
        nn.Dropout(p=dropout),
    ),
    'six': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 1, (4, 4), stride=(2, 1)),
        # F.relu,
        nn.Dropout(p=dropout),
    ),
    '11': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    '12': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 1, 3, stride=(2, 1)),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    '13': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu
    ),
    '14': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    '15': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    '16': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 2)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 2)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    '17': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 2)),
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu
    ),
    '18': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, dilation=1),
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu
    ),
    '19': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, dilation=2),
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu
    ),
    # Deep network, 4-fold
    '20': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # longer kernel in time
    '21': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, (6, 3), stride=(2, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # longer kernel in time
    '22': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, (6, 3), stride=(3, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # even longer kernel in time
    '23': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, (10, 3), stride=(5, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # dilation in frequency
    '24': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1), dilation=(1, 4)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # even more dilation in frequency
    '25': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, 3, stride=(2, 1), dilation=(1, 6)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
    # longer kernel in time, dilation in frequency
    '26': lambda dropout: nn.Sequential(
        nn.Conv2d(1, 10, (6, 3), stride=(3, 1), dilation=(1, 4)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, (6, 3), stride=(1, 1), dilation=(1, 4)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        nn.Conv2d(10, 10, 3, stride=(1, 1)),
        # F.relu,
        conv_1x1_bn(10, 1),
        nn.Dropout(p=dropout),
        # F.relu,
    ),
}


def get_shortening(conv_module, width):
    mapping = {}
    min_length = 10
    for l in range(10, 1200):
        x = torch.FloatTensor(np.random.random((1, 1, l, width)))
        try:
            op = conv_module(x)
        except RuntimeError:
            min_length = l
        else:
            mapping[l] = op.shape[-2]
    return width - op.shape[-1], mapping, min_length + 1


class RNNModel(SerializableModule):
    RNN_CLASS_DICT = {
        'LSTM': nn.LSTM,
        'GRU': nn.GRU,
    }

    def __init__(self, config):
        super(RNNModel, self).__init__()
        self.n_labels = config.get('n_labels', 1000)

        width = config['width']
        self.hidden_size = config['hidden_size']
        num_layers = config['num_layers']
        kind = config['kind']
        conv_dropout, second_dropout = config.get('dropout', (0.0, 0.0))
        self.dropout = nn.Dropout(p=second_dropout)
        # self.prelu = nn.PReLU()

        rnn_class = self.RNN_CLASS_DICT[kind]
        if config['conv'] == 'res':
            n_feature_maps = 6
            args = ([conv_bn(1, n_feature_maps, 2)] + [
                InvertedResidual(n_feature_maps, n_feature_maps, 1, 3, i) for i in range(3)
            ] + [conv_1x1_bn(n_feature_maps, 1)])
            conv = nn.Sequential(*args)
            shortening = get_shortening(conv, width)
            width -= shortening[0]
        elif config['conv']:
            conv = convs[config['conv']]
            if conv.__name__ == '<lambda>':
                conv = conv(conv_dropout)
            shortening = get_shortening(conv, width)
            width -= shortening[0]
        else:
            conv = None
            shortening = (0, 0, 1)
        self.drnn = DynamicRNN(rnn_class(
            width, self.hidden_size, num_layers, batch_first=True), conv=conv, shortening=shortening)
        self.linear = nn.Linear(self.hidden_size, self.n_labels)

    def forward(self, x):
        x = self.drnn(x)
        x = x.view(-1, self.hidden_size)
        # x = self.dropout(x)
        x = self.linear(x)
        # x = self.prelu(x)
        return F.softmax(x, dim=1)
