from collections import OrderedDict
import math

import torch.nn as nn

from .utils import SerializableModule


def conv_bn(inp, oup, stride):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )


def conv_1x1_bn(inp, oup):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
        # nn.BatchNorm2d(oup),
        # nn.ReLU6(inplace=True)
    )


class InvertedResidual(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio, setting_index):
        super(InvertedResidual, self).__init__()
        self.stride = stride
        self.setting_index = setting_index
        assert stride in [1, 2]

        hidden_dim = round(inp * expand_ratio)
        self.use_res_connect = self.stride == 1 and inp == oup
        if not self.use_res_connect:
            raise ValueError(f'No residual')

        if expand_ratio == 1:
            self.conv = nn.Sequential(
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride,
                          1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )
        else:
            self.conv = nn.Sequential(
                # pw
                nn.Conv2d(inp, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride,
                          1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )

    def forward(self, x):
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2(SerializableModule):
    def __init__(self, config):
        n_labels = config.get('n_labels', 1000)

        width = config['width']
        height = config['height']

        # input_size of the actual mobile net - 32*32*3
        input_size = 32
        first_channels = 1

        # To connect with mobile net
        very_first_layer = nn.Conv2d(
            1, first_channels, (height - input_size + 1, width - input_size + 1))

        # input_size = config.get('input_size', 224)
        width_mult = config.get('width_mult', 1.)

        super(MobileNetV2, self).__init__()
        block = InvertedResidual
        input_channel = 32
        # last_channel = 1280
        interverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]
        last_channel = 128
        interverted_residual_setting = [
            # t, c, n, s
            # [1, 16, 1, 1],
            [2, 2, 2, 2],
            [6, 24, 2, 2],
            [6, 24, 2, 2],
            [6, 24, 2, 2],
            # [6, 32, 3, 2],
            # [6, 64, 4, 2],
            # [6, 96, 3, 1],
            # [6, 160, 3, 2],
            # [6, 320, 1, 1],
        ]

        # building first layer
        assert input_size % 32 == 0
        input_channel = int(input_channel * width_mult)
        self.last_channel = int(
            last_channel * width_mult) if width_mult > 1.0 else last_channel
        self.features = [
            ('Converter', very_first_layer),
            ('first_layer', conv_bn(first_channels, input_channel, 2)),
        ]
        # building inverted residual blocks
        for setting_index, (t, c, n, s) in enumerate(interverted_residual_setting):
            output_channel = int(c * width_mult)
            layers = []
            for i in range(n):
                if i == 0:
                    layers.append(
                        block(input_channel, output_channel, s, t, setting_index))
                else:
                    layers.append(
                        block(input_channel, output_channel, 1, t, setting_index))
                input_channel = output_channel
            self.features.append(
                (f'invert_{setting_index}', nn.Sequential(*layers))
            )
        # building last several layers
        self.features.append(
            ('last', conv_1x1_bn(input_channel, self.last_channel)),
        )
        # make it nn.Sequential
        self.features = nn.Sequential(OrderedDict(self.features))
        # building classifier
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(self.last_channel, n_labels),
        )

        self._initialize_weights()

    def forward(self, x):
        x = x.unsqueeze(1)
        x = self.features(x)
        x = x.mean(3).mean(2)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()
