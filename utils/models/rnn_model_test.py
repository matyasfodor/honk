import numpy as np
import torch

from .model import _configs
from .rnn_model import RNNModel

def test_gru_conv():
    config2 = _configs['lstm']
    config2['hidden_size'] = 40
    config2['kind'] = 'GRU'
    config2['conv'] = True

    model = RNNModel(config2)

    x = [
        torch.FloatTensor(np.random.random((np.random.randint(40,70), 40))) for _ in range(10)
    ]

    y = model(x)

def test_gru_res():
    config = _configs['res-lstm']

    model = RNNModel(config)
    x = [
        torch.FloatTensor(np.random.random((np.random.randint(40,70), 40))) for _ in range(10)
    ]

    y = model(x)

if __name__ == "__main__":
    test_gru_res()
