from collections import OrderedDict
import torch
import torch.nn as nn


class SerializableModule(nn.Module):
    def __init__(self):
        super().__init__()

    def save(self, filename):
        torch.save(self.state_dict(), filename)

    def load(self, filename):
        dump_data = torch.load(
            filename, map_location=lambda storage, loc: storage)
        if 'state_dict' in dump_data:
            state_dict = dump_data['state_dict']
        else:
            state_dict = dump_data
        try:
            self.load_state_dict(state_dict)
        except RuntimeError as e:
            sd = OrderedDict()
            for k, v in state_dict.items():
                if k.startswith('lstm.'):
                    k = k.replace('lstm.', 'drnn.')
                sd[k] = v
            state_dict = sd
        try:
            self.load_state_dict(state_dict)
        except RuntimeError as e:
            sd = OrderedDict()
            for k, v in state_dict.items():
                if k.startswith('drnn.input_conv.'):
                    k = k.replace('drnn.input_conv.', 'drnn.conv.0.')
                sd[k] = v
            state_dict = sd
        self.load_state_dict(state_dict)
