import numpy as np
import torch

from .subsampled_rnn import SubsampledRNN
from .model import _configs

if __name__ == "__main__":
    config = _configs['s-rnn']
    # config['hidden_sizes'] = [12, 40]
    config['kind'] = 'GRU'
    model = SubsampledRNN(config)
    x = [
        torch.FloatTensor(np.random.random((np.random.randint(40,70), 40))) for _ in range(10)
    ]
    print(model)
    # config2 = _configs['lstm']
    # config2['hidden_size'] = 40
    # config2['kind'] = 'GRU'
    # model2 = LSTM(config2)
    # print(model2)
    # model.load('/Users/matyasfodor/sandbox/honk/model/gru_adam_03_2019-05-04T20:28:55.255438_0.91.pt')
    y = model(x)
