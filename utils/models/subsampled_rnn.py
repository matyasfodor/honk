import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from .drnn import DynamicRNN
from .rnn_model import get_shortening
from .utils import SerializableModule

def other(x):
    return x[::2]

class LDropout(nn.Module):
    def __init__(self, p):
        super(LDropout, self).__init__()
        self.dropout = nn.Dropout(p)

    def forward(self, x):
        return [self.dropout(e) for e in x]

class SubsampledRNN(SerializableModule):
    RNN_CLASS_DICT = {
        'LSTM': nn.LSTM,
        'GRU': nn.GRU,
    }

    def __init__(self, config):
        super(SubsampledRNN, self).__init__()
        self.n_labels = config.get('n_labels', 12)

        width = config['width']
        self.hidden_sizes = config['hidden_sizes']
        kind = config['kind']

        rnn_class = self.RNN_CLASS_DICT[kind]


        first_hs = self.hidden_sizes[0]
        self.last_hs = self.hidden_sizes[-1]
        middle_hs = self.hidden_sizes[1:-1]

        first_conv = nn.Sequential(
            nn.Conv2d(1, 1, (3, 3), (2, 1)),
            nn.Dropout(0.3),
        )

        shortening = get_shortening(first_conv, width)

        rnn_modules = [
            DynamicRNN(rnn_class(width - shortening[0], first_hs, 1, batch_first=True),
            only_last_output=False, conv=first_conv, shortening=shortening)
        ]

        input_size = first_hs
        for hs in middle_hs:
            rnn_modules.append(
                nn.Sequential(
                    DynamicRNN(
                        rnn_class(input_size, hs, 1, batch_first=True),
                        only_last_output=False,
                    ),
                    LDropout(0.3),
                )
            )
            input_size = hs

        rnn_modules.append(
            DynamicRNN(
                rnn_class(input_size, self.last_hs, 1, batch_first=True),
                only_last_output=True,
            )
        )

        self.rnn = nn.ModuleList(rnn_modules)
        self.linear = nn.Linear(self.last_hs, self.n_labels)

        self.subsample = other

    def subsample(self, x):
        return x

    def forward(self, x):
        for i, layer in enumerate(self.rnn):
            x = layer(x)
            if i < len(self.rnn) - 1:
                x = [self.subsample(d) for d in x]

        x = x.view(-1, self.last_hs)
        x = self.linear(x)
        return F.softmax(x, dim=1)
