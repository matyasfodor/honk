import numpy as np
import torch
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence, pad_sequence
import torch.nn.functional as F

# Source https://gist.github.com/quanguet/594c539b76fc52bef49ec2332e6bcd15


class DynamicRNN(nn.Module):
    """
    The wrapper version of recurrent modules including RNN, LSTM
    that support packed sequence batch.
    """

    def __init__(self, rnn_module, only_last_output=True, conv=None, shortening=(0, 0, 0)):
        super().__init__()
        self.rnn_module = rnn_module
        self.only_last_output = only_last_output
        self.conv = conv
        self.shortening = shortening

    def forward(self, x, initial_state=None):
        """
        Arguments
        ---------
        x : torch.FloatTensor
            padded input sequence tensor for RNN model
            Shape [batch_size, max_seq_len, embed_size]
        len_x : torch.LongTensor
            Length of sequences (b, )
        initial_state : torch.FloatTensor
            Initial (hidden, cell) states of RNN model.
        Returns
        -------
        A tuple of (padded_output, h_n) or (padded_output, (h_n, c_n))
            padded_output: torch.FloatTensor
                The output of all hidden for each elements. The hidden of padding elements will be assigned to
                a zero vector.
                Shape [batch_size, max_seq_len, hidden_size]
            h_n: torch.FloatTensor
                The hidden state of the last step for each packed sequence (not including padding elements)
                Shape [batch_size, hidden_size]
            c_n: torch.FloatTensor
                If rnn_model is RNN, c_n = None
                The cell state of the last step for each packed sequence (not including padding elements)
                Shape [batch_size, hidden_size]
        Example
        -------
        """
        min_len = self.shortening[2]
        x = [F.pad(r, (0, 0, 0, min_len - len(r)), 'constant', 0)
             if len(r) < min_len else r for r in x]
        len_x = torch.LongTensor([len(r) for r in x])
        x = pad_sequence(x, batch_first=True, padding_value=0.01)

        # First sort the sequences in batch in the descending order of length
        if self.conv is not None:
            original_length = x.shape[1]
            # Add channel of dim 1
            x = self.conv(x.unsqueeze(1)).squeeze(1)
            new_len = x.shape[1]
            print('Shortening: ', new_len)
            len_x = torch.LongTensor(
                [self.shortening[1][int(l)] for l in len_x])

        sorted_len, idx = len_x.sort(dim=0, descending=True)
        sorted_x = x[idx]

        # Convert to packed sequence batch
        packed_x = pack_padded_sequence(
            sorted_x, lengths=sorted_len, batch_first=True)

        # Check init_state
        if initial_state is not None:
            if isinstance(initial_state, tuple):  # (h_0, c_0) in LSTM
                hx = [state[:, idx] for state in initial_state]
            else:
                hx = initial_state[:, idx]  # h_0 in RNN
        else:
            hx = None

        # Do forward pass
        self.rnn_module.flatten_parameters()
        packed_output, last_s = self.rnn_module(packed_x, hx)

        # pad the packed_output
        max_seq_len = x.size(1)
        padded_output, length = pad_packed_sequence(
            packed_output, batch_first=True, total_length=max_seq_len)

        # Reverse to the original order
        _, reverse_idx = idx.sort(dim=0, descending=False)

        padded_output = padded_output[reverse_idx]

        if self.only_last_output:
            return padded_output[np.arange(len(padded_output)), len_x-1]
        else:
            return [
                padded_output[i, 0:len_x[i]]
                for i in range(padded_output.shape[0])
            ]
