import json
import requests

SLACK_URL = "https://hooks.slack.com/services/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"

HEADERS = {
    'Content-type': 'application/json',
}


def send_message(message):
    data = json.dumps({'text': message})
    resp = requests.post(SLACK_URL, headers=HEADERS, data=data)
    resp.raise_for_status()
