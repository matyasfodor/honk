from collections import ChainMap
import datetime
import argparse
import json
import os
import psutil
import random
import sys
import string
import subprocess
import warnings
import time

import numpy as np
import sentry_sdk
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.utils.data as data
from torchnet.meter import ConfusionMeter

from torch.utils.tensorboard import SummaryWriter

from .checkpoint import CheckpointManager
from utils.models import model as mod
from .magic_number import get_magic_list
from .local import LOCAL
import utils.datasets.speech_dataset as ds
import utils.datasets.speech_dataset_lstm as lds
import utils.datasets.speech_dataset_trimmed as dst
from utils.datasets.fluent_dataset import FluentDataset
from utils.datasets.manage_audio import AudioPreprocessor
from .slack_notification import send_message
from .utils import pairwise

from model_summary.model_summary import model_summary


def randomword(length):
    letters = string.ascii_lowercase
    return 'debug_' + ''.join(random.choice(letters) for i in range(length))


def progress(generator):
    if LOCAL:
        from tqdm import tqdm
        return tqdm(generator)
    return generator

# TODO - could be a bit more spohisticated, supports only the functionality that's being used.


class Timer:
    def __init__(self):
        self.start = time.time()

    def elapsed(self):
        new_ts = time.time()
        diff = new_ts - self.start
        self.start = new_ts
        return diff
# def get_timer():
#     start = time.time()
#     def inner():
#         new_ts = time.time()
#         diff = new_ts - start
#         start = new_ts
#         return diff
#     return inner


class Writer:
    def __init__(self, name):
        if not LOCAL:
            self.writer = SummaryWriter(name)

    def add_scalar(self, *args, **kwargs):
        if not LOCAL:
            self.writer.add_scalar(*args, **kwargs)
        else:
            print(*args, **kwargs)

    def add_graph(self, *args, **kwargs):
        if not LOCAL:
            self.writer.add_graph(*args, **kwargs)
        else:
            print(*args, **kwargs)


random_prefix = randomword(6)


class ConfigBuilder:
    def __init__(self, *default_configs):
        self.default_config = ChainMap(*default_configs)

    def build_argparse(self):
        parser = argparse.ArgumentParser()
        for key, value in self.default_config.items():
            key = "--{}".format(key)
            if isinstance(value, tuple):
                parser.add_argument(key,
                                    default=list(value),
                                    nargs=len(value),
                                    type=type(value[0]),
                                    )
            elif isinstance(value, list):
                parser.add_argument(key, default=value,
                                    nargs="+", type=type(value[0]))
            elif isinstance(value, bool) and not value:
                parser.add_argument(key, action="store_true")
            else:
                parser.add_argument(key, default=value, type=type(value))
        return parser

    def config_from_argparse(self, parser=None):
        if not parser:
            parser = self.build_argparse()
        known_args, unknown_args = parser.parse_known_args()
        if len(unknown_args) > 0:
            warnings.warn(Warning(f'Unknown args {unknown_args}'))
        args = vars(known_args)
        return ChainMap(args, self.default_config)


def evaluate_batch(scores, labels):
    batch_size = labels.size(0)
    accuracy = (torch.max(scores, 1)[1].view(
        batch_size).data == labels.data).float().sum() / batch_size
    return accuracy.item()


def conf_mx(scores, labels):
    batch_size = labels.size(0)
    pred = torch.max(scores, 1)[1].view(
        batch_size).data
    labels = labels.data
    num_labels = len(labels.unique())
    confusion_matrix = np.zeros([num_labels, num_labels])
    for p, l in zip(pred, labels):
        confusion_matrix[p, l] += 1
    return confusion_matrix


def set_seed(config):
    seed = config["seed"]
    torch.manual_seed(seed)
    np.random.seed(seed)
    if not config["no_cuda"]:
        torch.cuda.manual_seed(seed)
    random.seed(seed)


def drop_with_avg(tensor, expected_len):
    negative = get_magic_list(expected_len, len(tensor))
    negative = np.add.accumulate(negative)
    arr = np.setdiff1d(np.arange(len(tensor), dtype=int),
                       negative, assume_unique=True)
    arr = torch.LongTensor(arr)
    tensor = tensor[arr]
    return torch.Tensor(tensor)


def drop_nth(tensor, n):
    tensor = np.array(tensor)
    tensor = tensor[np.mod(np.arange(tensor.shape[0]) + 1, n) != 0]
    return torch.Tensor(tensor)


def drop_beginning(tensor, n):
    length = tensor.size()[0]
    drop = int((length * n) / 100.0)
    return tensor[drop:]


def drop_closest_percentage(tensor, n):
    tensor = np.array(tensor)
    distances = [float('inf')]
    for x_1, x_2 in pairwise(tensor):
        distances.append(np.linalg.norm(x_2 - x_1))
    distances = np.array(distances)
    sorted_dist = np.sort(distances)
    threshold = sorted_dist[int(len(sorted_dist)*n / 100.0)]
    tensor = tensor[distances > threshold]
    return torch.Tensor(tensor)

def drop_closest(tensor, threshold):
    tensor = np.array(tensor)
    distances = [float('inf')]
    for x_1, x_2 in pairwise(tensor):
        distances.append(np.linalg.norm(x_2 - x_1))
    distances = np.array(distances)
    tensor = tensor[distances > threshold]
    return torch.Tensor(tensor)

def use_lstm_loader(config):
    LSTM_models = (mod.ConfigType.LSTM.value,
                   mod.ConfigType.SubsampledRNN.value, mod.ConfigType.ResLSTM)
    return config['model'] in LSTM_models or config.get('use_lstm_loader', False)


def eval_subsample(reduction, model_in, config):
    original_lens = 0
    subsampled_lens = 0
    ret = []
    for v in model_in:
        initial_len = len(v)
        if reduction == 'drop_nth':
            val = drop_with_avg(v, config['drop_value'])
            # return [
            #     Variable(drop_nth(v, 4) if len(v) > 40 else v, requires_grad=False)
            #     for v in model_in]
        elif reduction == 'drop_nth_if':
            if initial_len > config['desired_len']:
                val = drop_nth(v, config['drop_value'])
            else:
                val = v
        elif reduction == 'drop_first_percent':
            val = drop_beginning(v, 10)
        elif reduction == 'drop_closest_percentage':
            val = drop_closest_percentage(v, config['drop_value'])
        elif reduction == 'drop_closest':
            val = drop_closest(v, config['drop_value'])
        elif reduction == 'dynamic_reduction':
            desired_len = float(config['desired_len'])
            scale_down = float(config['scale_down'])
            overflow = (initial_len - desired_len) / scale_down
            if overflow < 4:
                val = v
            else:
                rate = initial_len / overflow
                val = drop_with_avg(v, rate)
        else:
            val = v
        original_lens += initial_len
        subsampled_lens += len(val)
        ret.append(Variable(val , requires_grad=False))
    percent = (subsampled_lens * 100.0) / original_lens
    print(f'### Reduction: from {original_lens} to {subsampled_lens} - {100-percent:.2f}')
    return ret


def confusion_matrix(config, model=None, test_loader=None):
    data_set = ds.SpeechDataset
    if config['data_set'] == 'fluent':
        data_set = FluentDataset
    elif use_lstm_loader(config):
        data_set = lds.LSTMDataSet

    if not test_loader:
        training_set, dev_set, test_set = data_set.splits(config)
        test_loader = data.DataLoader(
            test_set,
            batch_size=len(test_set),
            collate_fn=test_set.collate_fn)
    if not model:
        model = mod.find_model(config['model'])(config)
        model.load(config["input_file"])
    if not config["no_cuda"]:
        set_cuda()
        model.cuda()
    model.eval()
    criterion = nn.CrossEntropyLoss()
    results = []
    total = 0
    reduction = config['reduction']
    for model_in, labels in progress(test_loader):
        if use_lstm_loader(config):
            model_in = eval_subsample(reduction, model_in, config)
            labels = torch.Tensor(np.array(labels)).long()
            batch_size = len(model_in)
        else:
            model_in = Variable(model_in, requires_grad=False)
            batch_size = model_in.size(0)

        if use_lstm_loader(config) and not config['model'] in (mod.ConfigType.LSTM.value, mod.ConfigType.SubsampledRNN.value):
            all_scores = []
            for (m_input, label) in zip(model_in, labels):
                m_iters = m_input.shape[0] - 32 + 1
                scores = []
                for offset in range(m_iters):
                    score = nn.Softmax(dim=1)(
                        model(m_input[offset:offset+32].unsqueeze(0)))
                    scores.append(score.squeeze().detach().numpy())
                scores = np.array(scores)
                scores = torch.Tensor(scores).mean(dim=0).unsqueeze(0)
                sample_acc = evaluate_batch(scores, label.unsqueeze(0))
                all_scores.append(sample_acc)
            all_scores = np.array(all_scores)
            final_acc = all_scores.mean()
            return final_acc

        if not config["no_cuda"]:
            model_in = model_in.cuda()
            labels = labels.cuda()

        scores = model(model_in)

        labels = Variable(labels, requires_grad=False)
        loss = criterion(scores, labels)
        hits = (1 - (torch.max(scores, 1)
                     [1].view(batch_size).data == labels.data).float()).numpy().astype(bool)
        # files_mistaken += list(fnames[hits])
        ret = conf_mx(scores, labels)
    return ret


def evaluate(config, model=None, test_loader=None):
    data_set = ds.SpeechDataset
    if config['data_set'] == 'fluent':
        data_set = FluentDataset
    elif use_lstm_loader(config):
        data_set = lds.LSTMDataSet
    # data_set = dst.SpeechDatasetTrimmed

    if not test_loader:
        training_set, dev_set, test_set = data_set.splits(config)
        # test_set = dev_set
        test_loader = data.DataLoader(
            test_set,
            batch_size=len(test_set),
            collate_fn=test_set.collate_fn)
    if not model:
        model = mod.find_model(config['model'])(config)
        model.load(config["input_file"])
    if not config["no_cuda"]:
        set_cuda()
        model.cuda()
    model.eval()
    criterion = nn.CrossEntropyLoss()
    results = []
    total = 0
    reduction = config['reduction']
    # files_mistaken = []
    # cf_meter = ConfusionMeter(config['n_labels'])
    for model_in, labels in progress(test_loader):
        # labels, fnames = zip(*labels)
        # fnames = np.array(fnames)
        if use_lstm_loader(config):
            model_in = eval_subsample(reduction, model_in, config)
            labels = torch.Tensor(np.array(labels)).long()
            batch_size = len(model_in)
        else:
            model_in = Variable(model_in, requires_grad=False)
            batch_size = model_in.size(0)

        if use_lstm_loader(config) and not config['model'] in (mod.ConfigType.LSTM.value, mod.ConfigType.SubsampledRNN.value):
            all_scores = []
            for (m_input, label) in zip(model_in, labels):
                m_iters = m_input.shape[0] - 32 + 1
                scores = []
                for offset in range(m_iters):
                    score = nn.Softmax(dim=1)(
                        model(m_input[offset:offset+32].unsqueeze(0)))
                    scores.append(score.squeeze().detach().numpy())
                scores = np.array(scores)
                scores = torch.Tensor(scores).mean(dim=0).unsqueeze(0)
                sample_acc = evaluate_batch(scores, label.unsqueeze(0))
                all_scores.append(sample_acc)
            all_scores = np.array(all_scores)
            final_acc = all_scores.mean()
            return final_acc

        if not config["no_cuda"]:
            model_in = model_in.cuda()
            labels = labels.cuda()

        # writer = SummaryWriter('runs/onlyastest')
        # writer.add_graph(model, model_in)
        # cf_meter.add(scores.detach(), labels)

        # Profiling https://pytorch.org/docs/stable/autograd.html#profiler
        # with torch.autograd.profiler.profile() as prof:
        #     scores = model(model_in)
        #     print(prof)
        scores = model(model_in)

        labels = Variable(labels, requires_grad=False)
        loss = criterion(scores, labels)
        hits = (1 - (torch.max(scores, 1)
                     [1].view(batch_size).data == labels.data).float()).numpy().astype(bool)
        # files_mistaken += list(fnames[hits])
        results.append(evaluate_batch(scores, labels) * batch_size)
        total += batch_size
    # print('Files mistaken', files_mistaken)
    # print(cf_meter.value())
    accuracy = sum(results) / total
    return accuracy


def get_output_filename(prefix, accuracy):
    ts = datetime.datetime.now().isoformat()
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "model", f'{prefix}_{ts}_{accuracy:.2f}.pt')


def set_cuda():
    torch.cuda.set_device(0)


def get_optimizer(config, model):
    if config['optimizer'] == 'SGD':
        return torch.optim.SGD(model.parameters(), lr=config["lr"][0], nesterov=config["use_nesterov"], weight_decay=config["weight_decay"], momentum=config["momentum"])
    elif config['optimizer'] == 'ADAM':
        return torch.optim.Adam(model.parameters(), lr=config["lr"][0], eps=1e-2, weight_decay=config["weight_decay"])
    else:
        raise ValueError(
            f'{config["optimizer"]} not defined in `get_optimizer`')


def train(config):
    checkpoints_path = os.path.join(os.path.dirname(
        os.path.realpath(__file__)), "..", "checkpoints")
    checkpoint_manager = CheckpointManager(
        checkpoints_path, config['output_prefix'])
    timer = Timer()
    writer = Writer(f'runs/{config["output_prefix"]}')
    output_dir = os.path.dirname(os.path.abspath(config["output_file"]))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    data_set = ds.SpeechDataset
    if config['data_set'] == 'fluent':
        data_set = FluentDataset
    elif use_lstm_loader(config):
        data_set = lds.LSTMDataSet

    train_set, dev_set, test_set = data_set.splits(config)
    print(f'Dataset loaded {timer.elapsed()}')
    # Put this behind a switch
    # data_set = dst.SpeechDatasetTrimmed

    model = mod.find_model(config['model'])(config)

    checkpoint = checkpoint_manager.last_model
    optimizer_state = None
    max_acc = 0
    start_epoch = 0

    if checkpoint is not None:
        start_epoch = checkpoint['epochs']
        max_acc = checkpoint['accuracy']
        dump = torch.load(checkpoint['model_filename'])
        optimizer_state = dump['optimizer']
        model.load_state_dict(dump['state_dict'])

        print(
            f'Training picked up at epoch {start_epoch} with accuracy {max_acc}')

    if not config["no_cuda"]:
        set_cuda()
        model.cuda()
    print(f'Model loaded {timer.elapsed()}')

    # Not working properly yet
    # writer.add_graph(model)

    optimizer = get_optimizer(config, model)
    if optimizer_state is not None:
        optimizer.load_state_dict(optimizer_state)

    schedule_steps = config["schedule"]
    schedule_steps.append(np.inf)
    sched_idx = 0
    criterion = nn.CrossEntropyLoss()

    train_loader = data.DataLoader(
        train_set,
        batch_size=config["batch_size"],
        shuffle=True, drop_last=True,
        collate_fn=train_set.collate_fn)
    dev_loader = data.DataLoader(
        dev_set,
        batch_size=min(len(dev_set), 16),
        shuffle=True,
        collate_fn=dev_set.collate_fn)
    test_loader = data.DataLoader(
        test_set,
        batch_size=min(len(test_set), 16),
        shuffle=True,
        collate_fn=test_set.collate_fn)

    time.time()
    train_len = len(train_loader)
    max_train_acc = 0
    batch_execution_times = []
    epoch_timer = Timer()
    process = psutil.Process(os.getpid())
    for epoch_idx in range(start_epoch, config["n_epochs"]):
        # print(f'# Epoch {epoch_idx} {epoch_timer.elapsed()}')
        running_loss = 0.0
        accs = []
        timer.elapsed()
        for batch_idx, (model_in, labels) in progress(enumerate(train_loader)):
            elapsed = timer.elapsed()
            batch_execution_times.append(elapsed)
            forecasted_epoch_time = train_len * \
                np.array(batch_execution_times).mean()
            mem_percent = process.memory_percent()
            # print(
                # f'## Batch {batch_idx} / {train_len} {elapsed} Forecasted epoch time: {forecasted_epoch_time} Mem percent: {mem_percent}')
            model.train()
            optimizer.zero_grad()
            labels = torch.Tensor(labels)

            if use_lstm_loader(config):
                reduction = config['reduction']
                if reduction == 'dynamic_reduction':
                    temp = []
                    desired_len = float(config['desired_len'])
                    scale_down = float(config['scale_down'])
                    for v in model_in:
                        initial_len = len(v)
                        overflow = (initial_len - desired_len) / scale_down
                        if overflow < 4:
                            temp.append(Variable(v, requires_grad=False))
                            continue
                        rate = initial_len / overflow
                        subsampled = drop_with_avg(v, rate)
                        temp.append(
                            Variable(subsampled, requires_grad=False))
                    model_in = temp
                else:
                    model_in = [Variable(m, requires_grad=False).float()
                                for m in model_in]
            else:
                model_in = Variable(model_in, requires_grad=False).float()

            if not config["no_cuda"]:
                if use_lstm_loader(config):
                    model_in = [m.cuda() for m in model_in]
                else:
                    model_in = model_in.cuda()
                labels = labels.cuda()

            scores = model(model_in)

            labels = Variable(labels, requires_grad=False).long()
            loss = criterion(scores, labels)
            loss.backward()

            # clip = 5
            # nn.utils.clip_grad_norm_(model.parameters(), clip)

            optimizer.step()
            running_loss += loss.item()
            if epoch_idx > schedule_steps[sched_idx]:
                sched_idx += 1
                print("### changing learning rate to {}".format(
                    config["lr"][sched_idx]))
                optimizer = torch.optim.SGD(model.parameters(), lr=config["lr"][sched_idx],
                                            nesterov=config["use_nesterov"], momentum=config["momentum"], weight_decay=config["weight_decay"])
            accs.append(evaluate_batch(scores, labels))
        if isinstance(train_loader, FluentDataset):
            train_set.reload_noise()
        train_loss = running_loss / len(train_set)
        train_acc = np.mean(accs)
        if max_train_acc < train_acc:
            max_train_acc = train_acc
            writer.add_scalar('data/maxTrainAcc', max_train_acc, epoch_idx)

        # if epoch_idx % config["dev_every"] == config["dev_every"] - 1:
        if True:
            model.eval()
            accs = []
            running_loss = 0.0
            for model_in, labels in dev_loader:
                # if config['model'] == mod.ConfigType.LSTM.value:
                    # labels = torch.Tensor([label for label in labels for _ in range(model_in.shape[1])]).long()
                if use_lstm_loader(config):
                    model_in = [Variable(m, requires_grad=False).float()
                                for m in model_in]
                else:
                    model_in = Variable(model_in, requires_grad=False).float()

                labels = torch.Tensor(labels)
                if not config["no_cuda"]:
                    if use_lstm_loader(config):
                        model_in = [m.cuda() for m in model_in]
                    else:
                        model_in = model_in.cuda()
                    labels = labels.cuda()

                scores = model(model_in)

                labels = Variable(labels, requires_grad=False).long()
                loss = criterion(scores, labels)
                running_loss += loss.item()
                accs.append(evaluate_batch(scores, labels))
            dev_acc = np.mean(accs)
            dev_loss = running_loss / len(dev_set)
            print(f'### Accuracies training: {train_acc:5.2f} dev: {dev_acc:5.2f} elapsed: {int(epoch_timer.elapsed())}')
            epoch_checkpoint = False
            if epoch_idx % 10 == 0:
                print(f'### Saving at epoch {epoch_idx} checkpoint')
                epoch_checkpoint = True
            is_best = False
            if dev_acc > max_acc:
                print("### saving best model")
                max_acc = dev_acc
                is_best = True
                writer.add_scalar('data/maxDevAcc', max_acc, epoch_idx)
            if epoch_checkpoint or is_best:
                checkpoint_manager.add_new(dict(
                    state_dict=model.state_dict(),
                    optimizer=optimizer.state_dict(),
                    epochs=epoch_idx,
                    accuracy=dev_acc,
                ))

        writer.add_scalar('data/trainAcc', train_acc, epoch_idx)
        writer.add_scalar('data/trainLoss', train_loss, epoch_idx)
        writer.add_scalar('data/devAcc', dev_acc, epoch_idx)
        writer.add_scalar('data/devLoss', dev_loss, epoch_idx)
    writer.add_scalar('data/testAcc', evaluate(config, model, test_loader))


def summary(config):
    model = mod.find_model(config['model'])(config)
    _, summary = model_summary(
        model, (50, 40), input_list=True, query_granularity=1)
    return summary


def summary_print(output):
    print(output)
    print(output.sum())


def main():
    output_file = os.path.join(os.path.dirname(
        os.path.realpath(__file__)), "..", "model", "model.pt")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model", choices=[x.value for x in list(mod.ConfigType)], default="cnn-trad-pool2", type=str)
    config, _ = parser.parse_known_args()

    global_config = dict(no_cuda=False, n_epochs=500, lr=[0.001], schedule=[np.inf], batch_size=64, dev_every=10, seed=0,
                         use_nesterov=False, input_file="", output_file=output_file, gpu_no=1, cache_size=32768, momentum=0.9, weight_decay=0.00001,
                         model=config.model, optimizer='SGD', reduction='', drop_value=0.0, desired_len=50, scale_down=1.0, use_lstm_loader=False)
    builder = ConfigBuilder(
        mod.find_config(config.model),
        ds.SpeechDataset.default_config(),
        global_config)
    parser = builder.build_argparse()
    parser.add_argument(
        '--type', choices=["train", "eval", "summary", "confusion"], default="train", type=str)
    parser.add_argument('--output_prefix', default=random_prefix, type=str)
    parser.add_argument('--data_set', default='speech', type=str)
    parser.add_argument('--ds_downsample', default=None, type=int)
    config = builder.config_from_argparse(parser)
    print('### Config:')
    print(config)
    print('#### Serializable config')
    print(json.dumps(dict(config)))
    print('###')
    set_seed(config)
    if config["type"] == "train":
        train(config)
    if config["type"] == "confusion":
        print(confusion_matrix(config))
    elif config["type"] == "summary":
        summary_print(summary(config))
    elif config["type"] == "eval":
        accuracy = evaluate(config)
        print(f'### final test accuracy: {accuracy}')
    send_message(f'training {config["output_prefix"]} finished')


if __name__ == "__main__":
    print('Script started')
    if not LOCAL:
        sentry_sdk.init(
            "https://1be7e5546ae3443f9e48f664747779ce@sentry.io/1443509")
        print('Sentry configured')
    try:
        main()
    except Exception as ex:
        if not LOCAL:
            send_message('Fail')
        raise ex

    print('Script finished')
