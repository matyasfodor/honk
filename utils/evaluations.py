import pandas as pd

from .train import evaluate, summary
from .models.model import find_model

CONFIG = {'no_cuda': True, 'n_epochs': 500, 'lr': [0.001], 'schedule': [float('inf')], 'batch_size': 64, 'dev_every': 1, 'seed': 0, 'use_nesterov': False, 'input_file': 'model/gru_dropout_asymm_03_2019-05-15T10:51:40.396554_0.94______2.p', 'output_file': '/Users/matyasfodor/sandbox/honk/utils/../model/model.pt', 'gpu_no': 1, 'cache_size': 32768, 'momentum': 0.9, 'weight_decay': 1e-05, 'model': 'lstm', 'optimizer': 'ADAM', 'reduction': '', 'desired_len': 50, 'scale_down': 1.0, 'use_lstm_loader': False, 'group_speakers_by_id': True, 'silence_prob': 0.1, 'noise_prob': 0.8, 'n_dct_filters': 40, 'input_length': 16000, 'n_mels': 40, 'timeshift_ms': 100, 'unknown_prob': 0.1, 'train_pct': 80, 'dev_pct': 10, 'test_pct': 10, 'wanted_words': ['yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go'], 'data_folder': '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02', 'audio_preprocess_type': 'normalized_logmel', 'n_labels': 12, 'width': 40, 'num_layers': 2, 'hidden_size': 40, 'kind': 'GRU', 'conv': True, 'dropout': [0.2, 0.2], 'type': 'eval', 'output_prefix': 'debug_ariatv', 'data_set': 'speech'}

configs = [
    # dict(name='res-gru-d11', conv='11'),
    # dict(name='res-gru-d12', conv='12'),
    # dict(name='res-gru-d13', conv='13'),
    # dict(name='res-gru-d14', conv='14'),
    # dict(name='res-gru-d15', conv='15'),
    dict(name='res-gru-d16', conv='16'),
    # dict(name='res-gru-d17', conv='17'),
    # dict(name='res-gru-d18', conv='18'),
    # dict(name='res-gru-d19', conv='19'),
]

if __name__ == "__main__":
    results = []
    for conf in configs:
        config = CONFIG.copy()
        config = {**config, **conf}
        model_cls = find_model(config['model'])
        # CopyClass = type(str(model_cls), model_cls.__bases__, dict(model_cls.__dict__))
        config['model_class'] = model_cls
        # try:
        df = summary(config)
        # except Exception:
        #     breakpoint()
        #     # continue
        results.append(dict(
            name=conf['name'],
            madd=df.sum()['MAdd'],
            params=df.sum()['parameter quantity'],
        ))
    df = pd.DataFrame(results)
    print(df)
