from utils.datasets.fluent_dataset import FluentDataset

from torch.utils.data import DataLoader

CONFIG = {
    'n_mels': 40,
    'n_dct_filters': 40,
}


def test_works():
    train, dev, test = FluentDataset.splits(CONFIG)
    d, l = train[0]
    print('data', d, 'label', l)


def test_ds():
    train, dev, test = FluentDataset.splits(CONFIG)

    dl = DataLoader(
        train,
        batch_size=60,
        shuffle=True, drop_last=True,
        collate_fn=train.collate_fn
    )
    breakpoint()
    v = next(dl)
    print(v)
