from enum import Enum
from itertools import tee
import numpy as np
import webrtcvad
from utils.utils import pairwise

SAMPLE_RATE = 16000


class Frame(object):
    """Represents a "frame" of audio data."""

    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, stride, audio, sample_rate):
    """Generates audio frames from PCM audio data.
    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.
    Yields Frames of the requested duration.
    """
    stride = int(sample_rate * (stride / 1000.0) * 2)
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = 0.0
    duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += duration
        offset += stride


def get_vad(signal, agressivity=3):
    vad = webrtcvad.Vad(agressivity)
    return np.array([vad.is_speech(f.bytes, SAMPLE_RATE)
                     for f in frame_generator(10, 10, signal, SAMPLE_RATE)])


def pattern_gen(inp):
    inp = inp.astype(int).astype(str)
    return pairwise('0' + ''.join(inp) + '0')


def get_patterns(sample):
    start_pattern = ('0', '1')
    end_pattern = ('1', '0')
    starts = []
    ends = []
    for i, pattern in enumerate(pattern_gen(sample)):
        if pattern == start_pattern:
            starts.append(i)
        if pattern == end_pattern:
            ends.append(i)
    return [e for e in zip(starts, ends)]


def get_sublists(signal):
    vad = get_vad(signal)
    full_len = len(vad)
    sublists = get_patterns(vad)
    # Just get the longes one for now
    start = None
    end = None
    try:
        start, stop = sorted(
            sublists, key=lambda element: element[1] - element[0], reverse=True)[0]
    except:
        pass
    return start, stop, full_len


class DatasetType(Enum):
    TRAIN = 0
    DEV = 1
    TEST = 2
