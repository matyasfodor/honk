from chainmap import ChainMap
import glob
import hashlib
import librosa
import math
import numpy as np
import random
import re
import os

import pandas as pd
import torch

from .dsutils import DatasetType
from .speech_dataset import SpeechDataset


class SpeechDatasetTrimmed(SpeechDataset):
    def __init__(self, data, set_type, config):
        label_data = {k: v['label'] for k, v in data.items()}
        super().__init__(label_data, set_type, config)
        self.trim_data = data
        self.height = config['height']
        self.required_len = ((self.height - 1) * 10) * 16

    def load_audio(self, example, silence=False):
        if silence:
            example = "__silence__"
        if random.random() < 0.7:
            try:
                return self._audio_cache[example]
            except KeyError:
                pass

        in_len = self.input_length
        if silence:
            data = np.zeros(in_len, dtype=np.float32)
        else:
            file_data = self._file_cache.get(example)
            if file_data is None:
                data = librosa.core.load(example, sr=16000)[0]
                trim_data = self.trim_data[example]
                start, stop = trim_data['start'], trim_data['stop']
                # If the file itself is too short, zero pad
                if len(data) < self.required_len:
                    extra = self.required_len - len(data)
                    left = math.ceil(extra / 2)
                    right = extra - left
                    data = np.pad(data, ((left, right,),), mode='constant')
                    # Just pass the next if
                    start = 0
                    stop = len(data)

                # If the VAD clip is not long enough, just expand it evenly - if possible
                if stop - start < self.required_len:
                    extra = self.required_len - (stop - start)
                    left = math.ceil(extra / 2)
                    if left > start:
                        left = start
                    right = extra - left
                    if right + stop > len(data):
                        right = len(data) - stop
                        left = extra - right
                    start -= left
                    stop += right
                data = data[start: stop]
                assert self.required_len <= len(data)
            else:
                data = file_data

            in_len = len(data)
            self._file_cache[example] = data

        if self.bg_noise_audio:
            bg_noise = random.choice(self.bg_noise_audio)
            a = random.randint(0, len(bg_noise) - in_len - 1)
            bg_noise = bg_noise[a:a + in_len]
        else:
            bg_noise = np.zeros(in_len)

        if random.random() < self.noise_prob or silence:
            a = random.random() * 0.1
            data = np.clip(a * bg_noise + data, -1, 1)

        # 16 frame / ms
        # 10ms stride, 25ms frame size
        # required_len = (((self.height - 1) * 10) + 25) * 16
        offset_range = in_len - self.required_len
        offset = random.randint(0, offset_range)

        data = data[offset: offset + self.required_len]

        self._audio_cache[example] = data
        return data

    def output_shape(self):
        return (1, self.height, 40,)

    @classmethod
    def splits(cls, config, csv_path='filtered_simple.csv'):
        folder = config["data_folder"]
        wanted_words = config["wanted_words"]
        unknown_prob = config["unknown_prob"]
        train_pct = config["train_pct"]
        dev_pct = config["dev_pct"]
        test_pct = config["test_pct"]

        words = {word: i + 2 for i, word in enumerate(wanted_words)}
        words.update({cls.LABEL_SILENCE: 0, cls.LABEL_UNKNOWN: 1})
        sets = [{}, {}, {}]
        unknowns = [0] * 3
        bg_noise_files = []
        unknown_files = []

        # TODO read background
        bg_noise_files = glob.glob(os.path.join(
            folder, '_background_noise_/*.wav'))

        data = pd.read_csv(os.path.join(os.path.dirname(__file__), csv_path))

        for command in data['command'].unique():
            label = words.get(command, words[cls.LABEL_UNKNOWN])

            for i, row in data[data['command'] == command].iterrows():
                path = row['path']
                fname = os.path.basename(path)
                path = os.path.join(folder, command, fname)
                if label == words[cls.LABEL_UNKNOWN]:
                    unknown_files.append(row)
                    continue
                if config["group_speakers_by_id"]:
                    hashname = re.sub(r"_nohash_.*$", "",
                                      os.path.basename(path))
                max_no_wavs = 2**27 - 1
                bucket = int(hashlib.sha1(hashname.encode()).hexdigest(), 16)
                bucket = (bucket % (max_no_wavs + 1)) * (100. / max_no_wavs)
                if bucket < dev_pct:
                    tag = DatasetType.DEV
                elif bucket < test_pct + dev_pct:
                    tag = DatasetType.TEST
                else:
                    tag = DatasetType.TRAIN
                sets[tag.value][path] = dict(
                    label=label, start=row['start'], stop=row['stop'])

        for tag in range(len(sets)):
            unknowns[tag] = int(unknown_prob * len(sets[tag]))
        random.shuffle(unknown_files)
        a = 0
        for i, dataset in enumerate(sets):
            b = a + unknowns[i]
            unk_dict = {}
            for u in unknown_files[a:b]:
                path = u['path']
                fname = os.path.basename(path)
                command = os.path.basename(os.path.dirname(path))
                path = os.path.join(folder, command, fname)
                unk_dict[path] = dict(
                    label=words[cls.LABEL_UNKNOWN], start=u['start'], stop=u['stop'])
            dataset.update(unk_dict)
            a = b

        train_cfg = ChainMap(dict(bg_noise_files=bg_noise_files), config)
        test_cfg = ChainMap(
            dict(bg_noise_files=bg_noise_files, noise_prob=0), config)
        datasets = (cls(sets[0], DatasetType.TRAIN, train_cfg), cls(sets[1], DatasetType.DEV, test_cfg),
                    cls(sets[2], DatasetType.TEST, test_cfg))
        return datasets
