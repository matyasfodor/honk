import contextlib
import librosa
import json
import wave

from .model_lstm import LSTM
from .speech_dataset import SpeechDataset
from .dsutils import DatasetType

files = {
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/on/0137b3f4_nohash_0.wav': 8,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/down/0bfec55f_nohash_1.wav': 5,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/left/54b6d355_nohash_3.wav': 6,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/seven/c1e0e8e3_nohash_3.wav': 1,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/off/a8cb6dda_nohash_0.wav': 9,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/off/f19c1390_nohash_3.wav': 9,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/left/ff63ab0b_nohash_0.wav': 6,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/right/ab46af55_nohash_2.wav': 7,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/up/195c120a_nohash_2.wav': 4,
    '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/stop/2144be19_nohash_0.wav': 10
}

config = {
    'weight_decay': 1e-05, 'momentum': 0.9, 'dnn1_size': 128, 'n_dct_filters': 40, 'lr': [0.001], 'audio_preprocess_type': 'MFCCs', 'height': 101,
    'dropout_prob': 0.5, 'n_feature_maps1': 54, 'conv1_size': [101, 8], 'input_file': '', 'cache_size': 32768,
    'data_folder': '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02', 'input_length': 16000, 'test_pct': 10, 'output_prefix': 'vkbmqa',
    'unknown_prob': 0.1, 'dev_pct': 10, 'data_set': 'speech', 'type': 'train', 'schedule': [float('inf')], 'train_pct': 80, 'width': 40, 'no_cuda': True,
    'batch_size': 64, 'n_labels': 12, 'gpu_no': 1, 'group_speakers_by_id': True, 'dnn2_size': 128, 'use_nesterov': False, 'n_epochs': 500, 'output_file':
    '/Users/matyasfodor/sandbox/honk/utils/../model/model.pt', 'model_class': LSTM,
    'noise_prob': 0.8, 'n_mels': 40, 'dev_every': 10, 'conv1_stride': [1, 1], 'conv1_pool': [1, 3], 'silence_prob': 0.1,
    'wanted_words': ['yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go'], 'model': 'cnn-one-fpool3', 'seed': 0, 'timeshift_ms': 100,
    'bg_noise_files': [
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/pink_noise.wav',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/white_noise.wav',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/README.md',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/dude_miaowing.wav',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/doing_the_dishes.wav',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/exercise_bike.wav',
        '/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/_background_noise_/running_tap.wav'
    ]
}


def test_vad():
    # with open('./test_params.json', 'r') as fp:
    #     files = json.load(fp)[0]
    sds = SpeechDataset(files, DatasetType.TRAIN, config)
    for s in sds:
        pass
    return


if __name__ == '__main__':
    test_vad()
    # librosa_vs_wave('/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/right/c1e0e8e3_nohash_3.wav')
