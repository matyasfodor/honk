from utils.datasets.speech_dataset_lstm import LSTMDataSet
import torch.utils.data as data

def test():
    config = dict(
        wanted_words=['yes', 'no', 'up', 'down', 'left', 'right', 'on', 'off', 'stop', 'go'],
        unknown_prob=.1,
        silence_prob=.1,
        noise_prob=.8,
        train_pct=80,
        dev_pct=10,
        test_pct=10,
        input_length=16000,
        timeshift_ms=0,
        group_speakers_by_id=True,
        cache_size=32768,
        n_mels=40,
        n_dct_filters=40,
        audio_preprocess_type='normalized_logmel',
        batch_size=64,
        data_folder='/Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02/',
    )
    (train_set, test_set, dev_set) = LSTMDataSet.splits(config, '/Users/matyasfodor/sandbox/honk/utils/test.csv')
    (train_set, test_set, dev_set) = LSTMDataSet.splits(config)
    loader = data.DataLoader(
        train_set,
        batch_size=3,
        shuffle=True, drop_last=True,
        collate_fn=train_set.collate_fn)
    for i in loader:
        pass

if __name__ == '__main__':
    test()
