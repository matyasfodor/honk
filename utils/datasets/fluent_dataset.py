import glob
import random
import numpy as np
import os
import wavefile

from chainmap import ChainMap
import pandas as pd
import torch.utils.data as data
import torch

from utils.datasets.manage_audio import AudioPreprocessor
from utils.datasets.speech_dataset import SpeechDataset, SimpleCache
from utils.datasets.dsutils import get_sublists, DatasetType

WW = ('alexa', 'hey google', 'computer')
OOV_PODCAST = 'oov-podcast'
OOV_CONFUSING = 'oov-confusing'

DATA_BASEPATH = '/users/spraak/spchdata/FluentWakeWords/fluent_generic_ww_2019/'
TRAIN_CSV = os.path.join(DATA_BASEPATH, 'data-train.csv')
VALIDATE_CSV = os.path.join(
    '/esat/spchtemp/scratch/r0473018/honk/fluent-filtered', 'data-validate.csv')
TEST_CSV = os.path.join(
    '/esat/spchtemp/scratch/r0473018/honk/fluent-filtered', 'data-test.csv')

NOISE_BASE_FOLDER = '/esat/spchtemp/scratch/r0473018/fluent_noise'

NOISES = [
    'babble',
    'freefield_mix',
    'mixed',
    'music',
    'speech',
    'street',
    'urbansounds_mix',
]

TRAIN_NOISE_FOLDERS = [
    os.path.join(NOISE_BASE_FOLDER, noise)
    for noise in NOISES
]

# TEST_NOISE = [
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/babble.wav',
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/freefield_mix.wav',
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/mixed.wav',
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/music.wav',
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/speech.wav',
#     '/users/spraak/spchdata/FluentWakeWords/noise/test/street.wav',
#     # '/users/spraak/spchdata/FluentWakeWords/noise/test/urbansounds_mix.wav',
# ]

FILENUMBER_PER_FOLDER = {
    path: len(glob.glob(os.path.join(path, '*')))
    for path in TRAIN_NOISE_FOLDERS
}

TRAIN_SNRS = [0.0, 5.0, 10.0]
TEST_SNRS = [5.0, 10.0, 15.0]

TRAINING_PROBABILITIES = {
    'ww': .75,
    'podcast': .95,
}

# TEST_PROBABILITIES = {
#     'ww': .3,
#     'podcast': .6,
# }


class VadError(Exception):
    pass


def load_audio(path):
    sr, data = wavefile.load(path)
    return data[0]


class RoundIterator:
    def __init__(self, df):
        self.df = df
        self.reset()

    def reset(self):
        self.iterator = iter(self.df.iterrows())

    def __iter__(self):
        return self

    def __next__(self):
        try:
            return next(self.iterator)
        except StopIteration:
            self.reset()
            return next(self)

    def __len__(self):
        return len(self.df)


def norm_factor(x):  # Avoid divide by zero
    xmax = np.abs(x).max()
    return 1 if xmax == 0 else 1.0 / xmax

# Do a full pass with looking for samples with no speech in it -> fitler them out.
#    -> On train, test validation
#    -> Add new csvs


class FluentDataset(data.Dataset):
    def __init__(self, csv_file, set_type, config):
        self._audio_cache = SimpleCache(config["cache_size"])
        self._file_cache = SimpleCache(config["cache_size"])

        self.LABELS = {w: i for i, w in enumerate(WW)}
        self.LABELS[OOV_CONFUSING] = len(WW)
        self.LABELS[OOV_PODCAST] = len(WW)
        self.set_type = set_type

        self.noise_folders = config['noise_folders']
        self.reload_noise()
        self.offset = 0

        self.snrs = config['snrs']
        self.audio_processor = AudioPreprocessor(
            n_mels=config["n_mels"], n_dct_filters=config["n_dct_filters"], hop_ms=10)

        df = pd.read_csv(csv_file)
        # # truncate for now
        if config['ds_downsample'] is not None:
            df = df.sample(frac=1).reset_index(drop=True)
            df = df.head(int(len(df) / config['ds_downsample']))

        ww = df[df['command'].isin(WW) & df['text'].isin(WW)]
        oov_podcast = df[df['command'] == OOV_PODCAST]
        oov_confusing = df[df['command'] == OOV_CONFUSING]

        # We only deal with 33% 33% 33% right now
        if self.set_type == DatasetType.TRAIN:
            self.probabilities = TRAINING_PROBABILITIES
            self.length = int(len(ww) / self.probabilities['ww'])
        else:
            ww = ww[ww['length'] > 0]
            oov_podcast = oov_podcast[oov_podcast['length'] > 0]
            oov_confusing = oov_confusing[oov_confusing['length'] > 0]
            self.length = len(ww) + len(oov_podcast) + len(oov_confusing)
            self.df = pd.concat([ww, oov_podcast, oov_confusing])

        self.inner_datasets = [
            RoundIterator(ww),
            RoundIterator(oov_podcast),
            RoundIterator(oov_confusing)
        ]

    def reload_noise(self):
        self.noise_files = []
        for folder in self.noise_folders:
            numbers = random.sample(range(FILENUMBER_PER_FOLDER[folder]), 50)
            for number in numbers:
                path = os.path.join(folder, f'chunk_{number}.wav')
                self.noise_files.append(load_audio(path))

    def collate_fn(self, data):
        x = []
        y = []
        for audio_data, label in data:
            preprocessed_audio = self.audio_processor.compute_normalized_logmel(
                audio_data)
            y.append(label)
            x.append(torch.Tensor(preprocessed_audio))
        return x, y

    def get_noise_of_length(self, length):
        noise = random.choice(self.noise_files)
        while len(noise) < length:
            noise = np.concatenate([noise, noise])
        offset = random.randint(0, len(noise) - length)
        return noise[offset: offset+length]

    def load_sample(self, path):
        if random.random() < 0.7:
            try:
                return self._audio_cache[path]
            except KeyError:
                pass

        signal = self._file_cache.get(path)
        if signal is None:
            signal = load_audio(path)
            start, end, full_len = get_sublists(signal)
            if start is None:
                raise VadError()
            frame_rate = int(len(signal) / full_len)
            signal = signal[start * frame_rate: end * frame_rate]
            if len(signal) == 0:
                original = load_audio(path)
                txt = f'Start {start}\n'
                txt += f'End {end}\n'
                txt += f'Original len {len(original)}\n'
                txt += f'Filename {path}'
                raise Exception(txt)
            self._file_cache[path] = signal

        noise = self.get_noise_of_length(len(signal))
        snr = random.choice(self.snrs)
        try:
            noise_gain = 10**(-snr/20.0) * norm_factor(noise)
            signal_gain = norm_factor(signal)
            mix = noise*noise_gain + signal * signal_gain
            self._audio_cache[path] = mix
        except ValueError:
            txt = f'Start {start}\n'
            txt += f'End {end}\n'
            txt += f'Noise len {len(noise)}\n'
            txt += f'Signal len {len(signal)}\n'
            raise Exception(txt)

        return mix

    def __len__(self):
        if self.set_type == DatasetType.TRAIN:
            return self.length
        else:
            return len(self.df)

    def __getitem__(self, index):
        if self.set_type == DatasetType.TRAIN:
            return self.get_train_item(index)
        else:
            return self.get_test_item(index)

    def get_test_item(self, index):
        row = self.df.iloc[index]
        label = self.LABELS[row['command']]
        sample = self.load_sample(
            os.path.join(DATA_BASEPATH, row['filename']))
        return sample, label

    def get_train_item(self, index):
        try:
            prob = random.random()
            ds_index = 2
            if prob < self.probabilities['ww']:
                ds_index = 0
            elif prob < self.probabilities['podcast']:
                ds_index = 1
            sample = None
            while sample is None:
                try:
                    _, row = next(self.inner_datasets[ds_index])
                    label = self.LABELS[row['command']]
                    sample = self.load_sample(
                        os.path.join(DATA_BASEPATH, row['filename']))
                except:
                    pass
            return sample, label
        except VadError:
            print(f'utterance {row["filename"]} skipped')
            return self[index]

    @classmethod
    def splits(cls, config):
        train_cfg = ChainMap(dict(
            noise_folders=TRAIN_NOISE_FOLDERS,
            snrs=TRAIN_SNRS,
        ), config)

        # Just use trian noise for now, can be switched later on
        validate_cfg = ChainMap(dict(
            noise_folders=TRAIN_NOISE_FOLDERS,
            snrs=TEST_SNRS,
        ), config)

        test_cfg = ChainMap(dict(
            noise_folders=TRAIN_NOISE_FOLDERS,
            snrs=TEST_SNRS,
        ), config)

        return [
            cls(TRAIN_CSV, DatasetType.TRAIN, train_cfg),
            cls(VALIDATE_CSV, DatasetType.DEV, validate_cfg),
            cls(TEST_CSV, DatasetType.TEST, test_cfg),
        ]
