import click
import os
import wavefile
import pandas as pd
from utils.datasets.dsutils import get_sublists, DatasetType

DATA_BASEPATH = '/users/spraak/spchdata/FluentWakeWords/fluent_generic_ww_2019/'

CSVS = {
    'test': os.path.join(DATA_BASEPATH, 'data-test.csv'),
    'train': os.path.join(DATA_BASEPATH, 'data-train.csv'),
    'validate': os.path.join(DATA_BASEPATH, 'data-validate.csv'),
}


@click.command(context_settings=dict(
    ignore_unknown_options=True,
    allow_extra_args=True,
))
@click.option('--kind', type=click.Choice(['test', 'train', 'validate']))
def main(kind):
    if kind not in CSVS:
        raise ValueError(f'{kind} not in {list(CSVS.keys())}')
    csv = CSVS[kind]
    df = pd.read_csv(csv)
    df['start'] = None
    df['stop'] = None
    df['length'] = None
    for i, row in df.iterrows():
        path = os.path.join(DATA_BASEPATH, row['filename'])
        sr, data = wavefile.load(path)
        data = data[0]
        start = None
        stop = None
        try:
            start, stop = get_sublists(data)
        except:
            print(f'{row["filename"]},{i},"no_sublists"')
        df.loc[i, 'start'] = start
        df.loc[i, 'stop'] = stop
        if start is not None:
            df.loc[i, 'length'] = stop - start

    os.makedirs('fluent-filtered', exist_ok=True)
    out_path = os.path.join('fluent-filtered', os.path.basename(csv))
    df.to_csv(out_path)


if __name__ == '__main__':
    main()
