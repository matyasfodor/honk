import glob
import os
import re
import subprocess

import click
import pandas as pd

BASE_PATH = '/esat/spchtemp/scratch/r0473018/honk/model'
TARGET_PATH = '/Users/matyasfodor/sandbox/honk/model/'

NAME_MATCHER = r'^(?P<name>[a-zA-Z0-9_-]*)_(?P<date>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d*)_(?P<accuracy>0\.\d{2})\.pt$'

def get_files(path, prefix):
    path = os.path.join(BASE_PATH, prefix) + '*.pt'
    glob.glob(path)
    with open(os.path.join(os.path.dirname(__file__), 'modelfiles.txt')) as fp:
        test_paths = [f.strip() for f in fp.readlines() if prefix in f]
    return test_paths

def get_all_files(prefixes):
    command = ['ssh', 'fasso', f'ls {BASE_PATH}']
    lines = subprocess.check_output(command).decode().split('\n')
    ret = []
    for l in lines:
        found = False
        for px in prefixes:
            found = l.startswith(px)
            if found:
                break
        if found:
            ret.append(l)
    return ret

@click.command()
@click.option('--prefixes', default='')
def main(prefixes):
    if prefixes == '':
        raise ValueError('## should not be empty')
    prefixes = [px for px in prefixes.split(' ') if px]
    files = get_all_files(prefixes)
    matches = []
    for f in files:
        match = re.match(NAME_MATCHER, f)
        if match:
            matches.append({**match.groupdict(), 'filename': f})
    df = pd.DataFrame(matches)
    df['date'] = pd.to_datetime(df['date'])
    df = df.loc[df.groupby(["name"])["date"].idxmax()]

    print(df)

    # paths = []
    # for i, row in df.iterrows():
    #     paths.append(os.path.join(BASE_PATH, row['filename']))
    # allpaths = ' '.join(paths)
    # print(f'Fetching {allpaths}')
    # command = ['scp', f'fasso:"{allpaths}"', TARGET_PATH]
    # print('\n\n')
    # print(' '.join(command))

if __name__ == '__main__':
    main()
