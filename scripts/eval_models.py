import csv
import os
import subprocess
import re

import click
import numpy as np

def hidden_tests(dry_run):
    PATHS = [
        ('gru_01_2019-05-02T13:45:47.143940_0.84.pt', 128),
        ('gru_adam_01_2019-05-03T03:41:02.512842_0.93.pt', 128),
        ('gru_adam_02_2019-05-04T02:43:13.585660_0.92.pt', 60),
        ('gru_adam_03_2019-05-04T20:28:55.255438_0.91.pt', 40),
        ('gru_adam_04_2019-05-05T20:46:40.410156_0.74.pt', 12),
        ('gru_adam_05_2019-05-05T17:58:05.604219_0.77.pt', 20),
        ('gru_adam_06_2019-05-05T19:28:06.637435_0.90.pt', 30),
        ('gru_adam_07_2019-05-05T17:57:47.344996_0.91.pt', 35),
    ]

    regex = r'^### final test accuracy: (?P<accuracy>0\.\d*)$'
    for i, (path, hidden_size) in enumerate(PATHS):
        command = f'python -m utils.train --type eval --input_file model/{path} --hidden_size {hidden_size} --audio_preprocess_type normalized_logmel --dropout_prob 0.0 --wanted_words yes no up down left right on off stop go --dev_every 1 --no_cuda --n_labels 12 --n_epochs 1200 --weight_decay 0.00001 --lr 0.01 --model lstm --kind GRU --data_folder /Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02'
        if dry_run:
            print(command)
            continue
        op = subprocess.check_output(command.split(' '))
        accuracy = None
        for line in op.decode('utf-8').split('\n'):
            match = re.match(regex, line)
            if match is not None:
                accuracy = float(match['accuracy'])
        if accuracy is None:
            print(op.decode('utf-8'))
        print(path, accuracy)

def test_frame_dropping(dry_run):
    acc_re = r'^### final test accuracy: (?P<accuracy>0\.\d*)$'
    speedup_re = r'^Speedup (?P<speedup>0\.\d*)$'
    with open('reduction_results.csv', 'w') as fp:
        writer = csv.writer(fp)
        writer.writerow(['desired_len', 'scale_down', 'accuracy', 'speedup'])
        cntr = 0
        for dl in np.arange(40, 61, 2):
            for sd in np.arange(1.0, 2.1, .5):
                command = f'python -m utils.train --type eval --input_file model/gru_adam_03_2019-05-04T20:28:55.255438_0.91.pt --hidden_size 40 --audio_preprocess_type normalized_logmel --dropout_prob 0.0 --wanted_words yes no up down left right on off stop go --dev_every 1 --no_cuda --n_labels 12 --n_epochs 1200 --weight_decay 0.00001 --lr 0.01 --model lstm --kind GRU --data_folder /Users/matyasfodor/susu/Thesis/baseline/speech_commands_v0.02 --reduction dynamic_reduction --desired_len {dl} --scale_down {sd}'
                if dry_run:
                    print(command)
                    continue
                op = subprocess.check_output(command.split(' '))
                accuracy = None
                speedup = None
                for line in op.decode('utf-8').split('\n'):
                    match = re.match(acc_re, line)
                    if match is not None:
                        accuracy = float(match['accuracy'])
                    match = re.match(speedup_re, line)
                    if match is not None:
                        speedup = float(match['speedup'])
                if accuracy is None:
                    print(op.decode('utf-8'))
                print([cntr, dl, sd, accuracy, speedup])
                writer.writerow([dl, sd, accuracy, speedup])
                cntr += 1
@click.command()
@click.option('--dry_run/--no_dry_run', default=False)
def main(dry_run):
    test_frame_dropping(dry_run)

if __name__ == '__main__':
    main()
