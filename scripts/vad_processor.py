import csv
import json
import contextlib
import glob
import numpy as np
from itertools import tee
from scipy.io import wavfile
from speechpy.feature import lmfe
from IPython.display import Audio, display
from time import sleep
import wave
import webrtcvad
import pandas as pd

class Frame(object):
    """Represents a "frame" of audio data."""
    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, stride, audio, sample_rate):
    """Generates audio frames from PCM audio data.
    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.
    Yields Frames of the requested duration.
    """
    stride = int(sample_rate * (stride/ 1000.0) * 2)
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = 0.0
    duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += duration
        offset += stride

def get_vads(path):
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        pcm_data = wf.readframes(wf.getnframes())
    vad_arrays = []
    for vad_aggressivity in range(4):
        vad = webrtcvad.Vad(vad_aggressivity)
        frames = list(frame_generator(10, 10, pcm_data, 16000))
        vad_array = np.array([vad.is_speech(f.bytes, 16000) for f in frames])
        vad_array = ''.join(vad_array.astype(int).astype(str))
        vad_arrays.append(vad_array)
    return vad_arrays
