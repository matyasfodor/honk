import click
import math
import os
import wavefile
from tqdm import tqdm

NOISE = [
    '/users/spraak/spchdata/FluentWakeWords/noise/train/babble.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/freefield_mix.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/mixed.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/music.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/speech.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/street.wav',
    '/users/spraak/spchdata/FluentWakeWords/noise/train/urbansounds_mix.wav',
]

OUTPUT_BASE_PATH = '/esat/spchtemp/scratch/r0473018/fluent_noise'


def saveWave(filename, data, samplerate, verbose=False):
    if verbose:
        print("Saving wave file:", filename)
    blockSize = 512
    channels, frames = data.shape
    fullblocks = frames // blockSize
    lastBlockSize = frames % blockSize
    with wavefile.WaveWriter(filename, channels=channels, samplerate=samplerate) as w:
        for i in range(fullblocks):
            w.write(data[:, blockSize*i:blockSize*(i+1)])
        if lastBlockSize:
            w.write(data[:, fullblocks*blockSize:])


@click.command(context_settings=dict(
    ignore_unknown_options=True,
    allow_extra_args=True,
))
@click.argument('file', type=click.Path(exists=True))
def main(file):
    print(f'Converting {file}')
    base_filename = os.path.splitext(os.path.basename(file))[0]
    file_folder = os.path.join(OUTPUT_BASE_PATH, base_filename)
    if not os.path.isdir(file_folder):
        os.mkdir(file_folder)
    sr, data = wavefile.load(file)
    print(f'{base_filename} {sr}')
    length = math.ceil(len(data[0]) / sr)
    for i in tqdm(range(length - 2)):
        start = i * sr
        end = (i + 2) * sr
        partial_data = data[:, start: end]
        op_filename = os.path.join(
            OUTPUT_BASE_PATH, base_filename, f'chunk_{i}.wav')
        saveWave(op_filename, partial_data, sr)


if __name__ == '__main__':
    main()
