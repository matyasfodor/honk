import csv
import os

import click
from scripts.vad_processor import get_vads

KEEP_TRACK_POSTFIX = '_keep_track'
ERROR_POSTFIX = '_error'
TRANSFORMED = '_transformed'

ORIGINAL_HEADERS = ['command', 'dataset', 'filename', 'language', 'length', 'ok', 'source', 'speaker', 'text']
ERROR_HEADERS = ['command', 'dataset', 'filename', 'error']
TRANSFORMED_HEADERS = ORIGINAL_HEADERS + ['vad_0', 'vad_1', 'vad_2', 'vad_3']

def write_header_if_missing(path, header):
    if not os.path.isfile(path):
        with open(path, 'w') as fp:
            writer = csv.writer(fp)
            writer.writerow(header)

def take(iterable, n):
    [next(iterable) for _ in range(n)]


@click.command(context_settings=dict(
    ignore_unknown_options=True,
    allow_extra_args=True,
))
@click.option('--input_csv')
@click.option('--basepath')
@click.option('--output_basepath')
@click.option('--tidy/--no-tidy', default=False)
@click.pass_context
def main(ctx, input_csv, basepath, output_basepath, tidy):
    # print(input_csv, basepath)
    current_path = os.getcwd()
    input_csv = os.path.join(current_path, input_csv)
    input_filename = os.path.basename(input_csv)
    input_base, ext = os.path.splitext(input_filename)

    keep_track = os.path.join(output_basepath, f'{input_base}{KEEP_TRACK_POSTFIX}{ext}')
    error = os.path.join(output_basepath, f'{input_base}{ERROR_POSTFIX}{ext}')
    transformed = os.path.join(output_basepath, f'{input_base}{TRANSFORMED}{ext}')

    if tidy:
        os.remove(keep_track)
        os.remove(error)
        os.remove(transformed)
        print('Removed output files.\nBye!')
        return

    write_header_if_missing(error, ERROR_HEADERS)
    write_header_if_missing(transformed, TRANSFORMED_HEADERS)

    try:
        with open(keep_track, 'r') as fp:
            rows_so_far = len(list(csv.reader(fp)))
    except FileNotFoundError:
        rows_so_far = 0

    with open(input_csv, 'r') as (ifp
         ), open(keep_track, 'a') as (keep_track_fp
         ), open(error, 'a') as (error_fp
         ), open(transformed, 'a') as transformed_fp:
        reader = csv.reader(ifp)
        header = next(reader)
        transformed_writer = csv.writer(transformed_fp)
        error_writer = csv.writer(error_fp)
        keep_track_writer = csv.writer(keep_track_fp)
        take(reader, rows_so_far)
        for i, row in enumerate(reader):
            n = i + rows_so_far

            if n % 500 == 0 and n > 0:
                print(f'### Fetched {n} tables so far')

            fname = row[2]
            try:
                vads = get_vads(os.path.join(basepath, fname))
                transformed_writer.writerow(row + vads)
            except Exception as e:
                error_writer.writerow(row[:3] + [str(e)])
            keep_track_writer.writerow([fname])



if __name__ == "__main__":
    main()
