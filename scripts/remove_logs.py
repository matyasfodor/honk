import click
import glob
import os
import shutil

DEFAULT_PATH = '/esat/spchtemp/scratch/r0473018/honk/'

@click.command()
@click.option('--dry_run/--run', default=False)
@click.option('--verbose/--no_verbose', default=False)
@click.option('--name', required=True)
@click.option('--path', default=DEFAULT_PATH)
def main(dry_run, verbose, name, path):
    if not name:
        raise ValueError

    outputs_path = os.path.join(path, 'outputs', name)
    runs_path = os.path.join(path, 'runs', name)
    if verbose:
        print('outputs_path', outputs_path)
        print('runs_path', runs_path)
    assert os.path.isdir(runs_path)
    g = glob.glob(outputs_path + '.*')
    if verbose:
        print('output paths:')
        print(g)
    assert len(g) == 3

    if verbose or dry_run:
        print('Delete:')
        print(runs_path)
    if not dry_run:
        shutil.rmtree(runs_path)

    for p in g:
        if verbose or dry_run:
            print(p)
        if not dry_run:
            os.remove(p)

if __name__ == "__main__":
    main()
