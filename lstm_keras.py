import keras
import keras.backend as K

import numpy as np
import tensorflow as tf


def flops_params(input_size, hidden_size):
    model = keras.Sequential()
    model.add(keras.layers.LSTM(hidden_size, return_sequences=True))
    # model.add(keras.layers.LSTM(512, return_sequences=True))

    run_meta = tf.RunMetadata()
    with tf.Session(graph=tf.Graph()) as sess:
        K.set_session(sess)
        model.build(input_shape=(1, 1, input_size))
        opts = tf.profiler.ProfileOptionBuilder.float_operation()
        # opts['output'] = 'none'
        flops = tf.profiler.profile(sess.graph, run_meta=run_meta, cmd='op', options=opts)

        opts = tf.profiler.ProfileOptionBuilder.trainable_variables_parameter()
        # opts['output'] = 'none'
        params = tf.profiler.profile(sess.graph, run_meta=run_meta, cmd='op', options=opts)
        flopts = flops.total_float_ops
        params = params.total_parameters
    return flopts, params

print(flops_params(140, 32))
