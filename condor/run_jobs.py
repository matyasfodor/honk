import fcntl, os
from collections import namedtuple
import csv

HEADERS = [
    'name',
    'command',
    'timestamp'
    'gitRev',
    'jobId',
    'success',
    'trainAcc',
    'devAcc',
    'testAcc',
]

Row = namedtuple('Row', HEADERS, defaults=[None for _ in HEADERS])

def merge_tuples(a, b):
    assert a.__class__ == b.__class__
    merged_dict = {
        **{k: v for k, v in a._asdict().items() if v is not None},
        **{k: v for k, v in b._asdict().items() if v is not None},
    }
    return a.__class__(**merged_dict)

def run(rows):
    original_rows = rows[:]
    rows = [Row(*row) for row in rows]
    newRows = []
    for row in rows:
        if row.jobId is None:
            try:
                print(f"Let's run this beautiful job {row.name}")
                breakpoint()
            except Exception as ex:
                print(f'Failed to launch job with name {row.name}')
    return original_rows

def main():
    with open('runlist.csv', 'r+') as fp:
        fcntl.flock(fp.fileno(), fcntl.LOCK_EX)
        reader = csv.reader(fp)
        rows_in = [r for r in reader]
        # print(reader)
        # rows_out = [[r[i] if i < len(r) else 'Duupe' for i in range(3)] for r in reader]
        rows_out = run(rows_in)
        # rows = [[r if True else 'Duupe' for i in range(len(3))] for r in reader]
        fp.seek(0)
        writer = csv.writer(fp)
        writer.writerows(rows_out)
        fp.truncate()
        fcntl.lockf(fp, fcntl.LOCK_UN)

if __name__ == '__main__':
    main()
