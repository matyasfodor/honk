pull:
	ssh fasso "cd /esat/spchtemp/scratch/r0473018/honk && git pull"

sync:
	rsync --update -vv -a fasso:/esat/spchtemp/scratch/r0473018/honk/runs/* ~/sandbox/honk/runs/

tb: sync
	tensorboard --logdir runs/ --host localhost

rm-logs:
	echo "$$name"
	ssh fasso "pyenv activate baseline && python /esat/spchtemp/scratch/r0473018/honk/scripts/remove_logs.py --verbose --name $$name"

notebook:
	ssh fasso "pyenv activate baseline && cd /esat/spchtemp/scratch/r0473018/honk/ && jupyter notebook --no-browser --port 9100"

stat:
	ssh fasso "condor_q"
