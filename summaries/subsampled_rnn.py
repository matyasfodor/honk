from model_summary.model_summary import model_summary
from utils.models.model import SpeechModel, _configs
from utils.models.subsampled_rnn import SubsampledRNN

model = SubsampledRNN({
  'n_labels': 12,
  'width': 40,
  'hidden_sizes': [40, 40, 40],
  'no_cuda': True,
  'kind': 'GRU',
  'conv': 'res'
})

_, lstm_summary = model_summary(model, (50, 40), input_list=True, query_granularity=1)
print(lstm_summary)
print(lstm_summary.sum())
