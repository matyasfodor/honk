from model_summary.model_summary import model_summary
from utils.models.model import SpeechModel, _configs
from utils.models.rnn_model import RNNModel

model = RNNModel({
  'n_labels': 12,
  'width': 40,
  'hidden_size': 40,
  'num_layers': 2,
  'no_cuda': True,
  'kind': 'GRU',
  'conv': True,
  'dropout': [0.0, 0.0],
})

_, lstm_summary = model_summary(model, (50, 40), input_list=True, query_granularity=1)
print(lstm_summary)
print(lstm_summary.sum())
