import math
import torch as th
import numpy as np
import torch.nn as nn
from model_summary.model_summary import model_summary
from utils.drnn import DynamicRNN

from thop import profile

class LSTM2(nn.Module):

    def __init__(self, input_size, hidden_size, bias=True):
        super(LSTM2, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.bias = bias
        self.i2h = nn.Linear(input_size, 4 * hidden_size, bias=bias)
        self.h2h = nn.Linear(hidden_size, 4 * hidden_size, bias=bias)
        self.reset_parameters()

    def reset_parameters(self):
        std = 1.0 / math.sqrt(self.hidden_size)
        for w in self.parameters():
            w.data.uniform_(-std, std)

    def forward(self, x, hidden=None):
        if hidden is None:
            zeros = th.zeros(self.hidden_size, 1)
            hidden = (zeros, zeros)
        h, c = hidden
        h = h.view(h.size(1), -1)
        c = c.view(c.size(1), -1)
        x = x.view(x.size(1), -1)

        # Linear mappings
        preact = self.i2h(x) + self.h2h(h)

        # activations
        gates = preact[:, :3 * self.hidden_size].sigmoid()
        g_t = preact[:, 3 * self.hidden_size:].tanh()
        i_t = gates[:, :self.hidden_size]
        f_t = gates[:, self.hidden_size:2 * self.hidden_size]
        o_t = gates[:, -self.hidden_size:]

        c_t = th.mul(c, f_t) + th.mul(i_t, g_t)

        h_t = th.mul(o_t, c_t.tanh())

        h_t = h_t.view(1, h_t.size(0), -1)
        c_t = c_t.view(1, c_t.size(0), -1)
        return h_t, (h_t, c_t)

input_size = 140
hidden_size = 32

model = LSTM2(input_size=input_size, hidden_size=hidden_size)
lstm_summary = model_summary(model, (1, input_size))

print('########, Other model')
model2 = DynamicRNN(th.nn.LSTM(input_size, hidden_size, batch_first=True))
lstm_summary2 = model_summary(model2, (1, input_size))


# print('Thop artisian')
# flops, params = profile(model, input_size=(1, 1, input_size))
# print('flops', flops, 'params', params)

# print('Thop built-in')
# flops, params = profile(model2, input_size=(1, 1, input_size))
# print('flops', flops, 'params', params)