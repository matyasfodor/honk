from model_summary.model_summary import model_summary
from utils.model_lstm import LSTM

model = LSTM({'n_labels': 12, 'width': 40, 'hidden_size': 128, 'num_layers':2, 'no_cuda': True, 'kind': 'LSTM'})
lstm_summary = model_summary(model, (1, 40))