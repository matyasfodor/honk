# V2
from model_summary.model_summary import model_summary
from utils.mobilenetv2_model import *

model = MobileNetV2({'n_labels': 12, 'width': 40, 'height': 101})
summary_mobilev2 = model_summary(model, (101, 40), query_granularity=4)
