from model_summary.model_summary import model_summary
from utils.models.model import SpeechModel, _configs
from utils.models.rnn_model import RNNModel

import pandas as pd

input_features = 40
hidden_size = 40
length = 50

def get_stats(input_features, hidden_size, length):
    # n_labels=12, width=40, num_layers=2, hidden_size=512, kind='LSTM'
    conf = _configs['lstm']
    conf['hidden_size'] = hidden_size
    conf['kind'] = 'GRU'
    conf['width'] = input_features

    model = RNNModel(conf)
    _, summary_speechnet = model_summary(model, (length, input_features))
    total_madds = summary_speechnet['MAdd'].sum()
    params = summary_speechnet['parameter quantity'].sum()
    return total_madds, params

print(f'Stats for gru_adam runs assuming input length of {length}')
res = []

pd.options.display.float_format = '${:,.2f}'.format
for hidden_size in [128, 60, 40, 12, 20, 30, 35]:
    madds, params = get_stats(input_features, hidden_size, length)
    res.append(dict(
        madds='{:,.2f}'.format(madds),
        params='{:,.2f}'.format(params),
        hidden_size=hidden_size
    ))

df = pd.DataFrame(res)
print(df)