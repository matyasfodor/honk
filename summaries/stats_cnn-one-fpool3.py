from model_summary.model_summary import model_summary
from utils.models.model import SpeechModel, _configs

speechnet_conf = _configs['cnn-one-fpool3']
speechnet_conf['n_labels'] = 10

SN = SpeechModel(speechnet_conf)
_, summary_speechnet = model_summary(SN, (32, 40))
print(summary_speechnet)
print(summary_speechnet.sum())
